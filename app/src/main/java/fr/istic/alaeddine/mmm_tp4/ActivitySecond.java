package fr.istic.alaeddine.mmm_tp4;

import android.content.Intent;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;

import java.util.List;

/**
 * Created by alaeddine on 20/01/17.
 */

public class ActivitySecond extends AppCompatActivity {

    private DAO datasource;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        ListView lv= (ListView)findViewById(R.id.listview);

        datasource = new DAO(this);
        datasource.open();

        List<Client> clients = datasource.getAllClients();
        MatrixCursor c = new MatrixCursor(new String[]{
                Content.ID,
                Content.PRENOM,
                Content.NOM,
                Content.DATE,
                Content.VILLE
        });

        for (int i = 0; i < clients.size(); i++) {


            c.newRow()
                    .add(i)
                    .add(clients.get(i).getPrenom())
                    .add(clients.get(i).getNom())
                    .add(clients.get(i).getDate())
                    .add(clients.get(i).getVille());

        }


        // 4.3) create your own adapter
        ClassCursorAdapter adapter = new ClassCursorAdapter(this,c, 0);

        // 5) link the adapter to the listview
        lv.setAdapter(adapter);


    }

    public void redirect(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
