
package fr.istic.alaeddine.mmm_tp4;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MySQLiteHelper extends SQLiteOpenHelper {

	public static final String TABLE_CLIENTS = "clients";

	static final String ID = "_ID";
	static final String PRENOM = "Prenom";
	static final String NOM = "Nom";
	static final String VILLE = "Ville";
	static final String DATE = "Date";

	private static final String DATABASE_NAME = "clients1.db";
	private static final int DATABASE_VERSION = 1;

	// Database creation sql statement
	private static final String DATABASE_CREATE = "create table "
			+ TABLE_CLIENTS + "( " + ID
			+ " integer primary key autoincrement, "
			+ PRENOM + " text not null, "
			+ NOM + " text not null, "
			+ DATE + " text not null, "
			+ VILLE + " text  not null"
			+");";

	public MySQLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {

		System.out.println("CREATE");
		database.execSQL(DATABASE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(MySQLiteHelper.class.getName(),
				"Upgrading database from version " + oldVersion + " to "
						+ newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_CLIENTS);
		onCreate(db);
	}

}
