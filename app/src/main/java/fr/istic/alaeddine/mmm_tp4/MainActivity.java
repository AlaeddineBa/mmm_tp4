package fr.istic.alaeddine.mmm_tp4;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    private DAO datasource;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        datasource = new DAO(this);
        datasource.open();


        final Button wiki = (Button) findViewById(R.id.wikipedia);
        wiki.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String ville="";
                TextView t4 = (TextView)findViewById(R.id.Ville);
                ville += t4.getText();
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://fr.wikipedia.org/"+ville));
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){

        int id = item.getItemId();
        if(R.id.button_reset == id)
            remiseZero();
        else if(R.id.ajout_numero == id)
            ajouterNumero();

        return true;

    }

    public void redirect(View view) {
        Client client = null;

        TextView t1 = (TextView)findViewById(R.id.Nom);

        TextView t2 = (TextView)findViewById(R.id.Prenom);

        TextView t3 = (TextView)findViewById(R.id.Date);

        TextView t4 = (TextView)findViewById(R.id.Ville);


        // Save to the database
        client = datasource.createClient(t1.getText().toString(), t2.getText().toString(), t3.getText().toString(), t4.getText().toString());


        datasource.close();
        Intent intent = new Intent(this, ActivitySecond.class);
        startActivity(intent);
    }

    public void remiseZero(){
        TextView t1 = (TextView)findViewById(R.id.Nom);
        t1.setText("");
        TextView t2 = (TextView)findViewById(R.id.Prenom);
        t2.setText("");
        TextView t3 = (TextView)findViewById(R.id.Date);
        t3.setText("");
        TextView t4 = (TextView)findViewById(R.id.Ville);
        t4.setText("");
    }

    public void ajouterNumero(){
        RelativeLayout l = (RelativeLayout) findViewById(R.id.activity_main);
        TextView telephone = new TextView(this.getApplicationContext());
        telephone.setText("Telephone");
        l.addView(telephone);
    }



}
