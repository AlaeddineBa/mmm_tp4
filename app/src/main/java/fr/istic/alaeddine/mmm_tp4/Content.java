package fr.istic.alaeddine.mmm_tp4;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.support.annotation.Nullable;

import java.util.List;

/**
 * Created by alaeddine on 02/02/17.
 */

public class Content {


    static final String ID = "_ID";
    static final String PRENOM = "Prenom";
    static final String NOM = "Nom";
    static final String DATE = "Date";
    static final String VILLE = "Ville";


}
